import { showAlert } from "./alert.js"

const login = async (email, password, role) => {
    try {
        const res = await axios({
            method: 'POST',
            url: 'http://localhost:4000/api/v1/users/login',
            data: {
                email,
                password,
                 
                
            },
        })
        // console.log(res)
        const roles = res.data.data.user.role
        // console.log(roles)
        if (res.data.status === 'success'){

            if(role === roles){
                if(roles  == "buyer"){
                    window.alert("Login Successful for buyer")
                }
        
                else if(roles  == "seller"){
                    window.alert("Login Successful for seller")
                }
                else if(roles  == "admin"){
                    window.alert("Login Successful admin")
                }
                
            }
            else{
                window.alert("Invalid Password or Email or User Type")
            }

        }
        else{
            window.alert("Invalid Password or Email or User Type")
        }


        
        // if (res.data.status == 'success') {

            
        //     showAlert('success', 'Logged in successfully')
        //     window.setTimeout(() => {
        //         location.assign('/')
        //     }, 1500)
        //     // var obj = res.data.data.user
        //     // // console.log(obj)
        //     // document.cookie = ' token = ' + JSON.stringify(obj)
        //     // // console.log(obj)
        // }
    } catch (err) {
        let message = 
            typeof err.response !== 'undefined'
            ? err.response.data.message
            :err.message
        showAlert('error', 'Error: Incorrect email or password', message)
    }
}

console.log("Js connected")
document.querySelector('.form').addEventListener('submit', (e) => {
    e.preventDefault()
    const email = document.getElementById('email').value
    const password = document.getElementById('password').value
    const selectedRole = document.querySelector('input[name="role"]:checked').value;

    // Do something with the selected role
    // console.log("Selected role: " + selectedRole);
      


    // console.log(email,password,selectedRole)
    login(email, password,selectedRole)
})
