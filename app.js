const express = require('express');
const path = require('path');
const userRouter = require('./routes/userRoutes');
const viewRouter = require('./routes/viewRoutes');

const app = express();

app.use(express.json());

// Use the userRouter and viewRouter as middleware
app.use('/api/v1/users', userRouter);
app.use('/', viewRouter);

app.use(express.static(path.join(__dirname, 'views')));

// Your other configuration and code

module.exports = app;
