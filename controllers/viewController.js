const path = require('path')

/* LOG IN PAGE*/
exports.getLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'signin.html'))
}

/* ADMIN LOG IN PAGE*/
exports.getAdminLoginForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'adminlogin.html'))
}
/* SIGN UP PAGE*/
exports.getSignupForm = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'signup.html'))
}

/* HOME PAGE*/
exports.getHome = (req, res) => {
    res.sendFile(path.join(__dirname, '../', 'views', 'landing.html'))
}
